from rx import Observable

class AM2302():
    def __init__(self, channel, dht):
        self.channel = channel
        self.stream = None
        self.__dht = dht

    def read_value(self, index):
        return self.__dht.read(self.__dht.AM2302, self.channel)

    def are_all_readings_valid(self, readings):
        return all(reading is not None for reading in readings)

    def stream_readings(self, period=2000, scheduler=None):
        if self.stream is None:
            self.stream = Observable.interval(period, scheduler=scheduler) \
                .map(self.read_value) \
                .filter(self.are_all_readings_valid)

        return self.stream
