"""
    LED
    This is the class for setting up an LED and handling its status.

    We setup and clean the channel whenever create and destroy the channel.
    This is also done when we change the channel for a channel
"""
from .led_mode import LEDMode

class LED():
    def __init__(self, gpio, channel=None, mode=LEDMode.OFF):
        self.__gpio = gpio
        self.__gpio.setmode(self.__gpio.BCM)
        self.channel = channel
        self.mode = mode

    def __del__(self):
        self.cleanup_channel(self.channel)

    def cleanup_channel(self, channel):
        if channel is not None:
            self.__gpio.cleanup(channel)

    def update_device(self, channel, mode=LEDMode.OFF):
        self.__gpio.output(channel, mode.value)

    @property
    def channel(self):
        return self.__channel

    @property
    def mode(self):
        return self.__mode

    @channel.setter
    def channel(self, channel):
        if hasattr(self, 'channel'):
            self.cleanup_channel(self.channel)

        self.__channel = channel
        self.__gpio.setup(self.channel, self.__gpio.OUT)

    @mode.setter
    def mode(self, mode):
        if not isinstance(mode, LEDMode):
            raise ValueError
        else:
            self.__mode = mode
            self.update_device(self.channel, self.mode)

    def turn_on(self):
        self.mode = LEDMode.ON

    def turn_off(self):
        self.mode = LEDMode.OFF

    def toggle(self):
        self.turn_on() if self.mode == LEDMode.OFF else self.turn_off()
