"""
    Mode
    Enum that relates the status of the LED to the voltage on the device itself
"""
from enum import Enum
import RPi.GPIO as GPIO

class LEDMode(Enum):
    ON = GPIO.HIGH
    OFF = GPIO.LOW
