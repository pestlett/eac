import Adafruit_DHT as DHT

from devices.sensors.dht.am2302 import AM2302

class EnvironmentController():
    def __init__(self, channel):
        self.__outputs = []
        self.__temperature = AM2302(channel=channel, dht=DHT)
        self.__stream = self.__temperature.stream_readings() \
            .publish()

    def __del__(self):
        while len(self.__outputs) > 0:
            self.__remove_and_unsubscribe_output(self.__outputs[0])

    def __remove_and_unsubscribe_output(self, output):
        self.__outputs.remove(output)
        output.get('subscription', {}).unsubscribe()

    def __are_outputs_the_same(self, output):
        return lambda item: item.get('output') == output

    def connect(self):
        self.__stream.connect()

    def add_output(self, output):
        self.__outputs.append({
            'output': output,
            'subscription': self.__stream.subscribe(output.write)
        })

    def remove_output(self, output):
        output_dict = list(filter(self.__are_outputs_the_same(output), self.__outputs))

        if output_dict:
            self.__remove_and_unsubscribe_output(output_dict.pop())
