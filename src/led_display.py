import RPi.GPIO as GPIO

from devices.diodes.led import LED
from devices.diodes.led_mode import LEDMode

class LEDDisplay():
    def __init__(self):
        self.__leds = []

    def __del__(self):
        for led in self.__leds:
            led.__del__()

    def __get_current_leds_channels_list(self):
        return list(map(lambda led: led.channel, self.__leds))

    def __get_nth_led_in_list(self, index):
        return self.__leds[index]

    def add_led(self, led):
        if not isinstance(led, LED):
            raise TypeError

        if led.channel in self.__get_current_leds_channels_list():
            raise ValueError('Channel {} is already in the list of LEDs'.format(led.channel))

        self.__leds.append(led)

    def add_led_by_channel(self, channel):
        self.add_led(LED(channel=channel, mode=LEDMode.OFF, gpio=GPIO))

    def get_leds_currently_off(self):
        return list(filter(lambda led: led.mode == LEDMode.OFF, self.__leds))

    def get_leds_currently_on(self):
        return list(filter(lambda led: led.mode == LEDMode.ON, self.__leds))

    def turn_off_all_leds(self):
        for led in self.__leds:
            led.turn_off()

    def turn_on_all_leds(self):
        for led in self.__leds:
            led.turn_on()

    def turn_on_led_by_index(self, index):
        led = self.__get_nth_led_in_list(index)

        led.turn_on()

    def turn_off_led_by_index(self, index):
        led = self.__get_nth_led_in_list(index)

        led.turn_off()

    def for_each(self, action):
        for led in self.__leds:
            action(led)
