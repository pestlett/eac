from environment_controller import EnvironmentController

from outputs.influxdb import InfluxDb
from outputs.led_panel import LEDPanel
from outputs.console import Console

outputs = {}
environment = None

def setup(channels):
    environment = EnvironmentController(channel=21)
    outputs.update({
        'influx_db': InfluxDb(),
        'led_panel': LEDPanel(channels=[16, 20, 12, 25, 24, 23]),
        'console': Console()
    })

def main():
    for key, value in outputs.items():
        environment.add_output(value)

    environment.connect()

if __name__ == '__main__':
    setup()
    main()
