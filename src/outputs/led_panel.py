import math

from led_display import LEDDisplay

class LEDPanel():
    temperature_ranges = [range(-274, 5), range(5, 16), range(16, 20), range(21, 23), range(23, 25), range(25, 274)]

    def __init__(self, display=LEDDisplay, channels=[]):
        self.__display = display()
        self.__add_leds_by_channel(channels)

    def __add_leds_by_channel(self, channels):
        for channel in channels:
            self.__display.add_led_by_channel(channel)

    def get_led_index_based_on_temperature(self, temp, temp_ranges):
        index = [index for index, temp_range in enumerate(temp_ranges) if temp in temp_range]

        return None if not index else index.pop()

    def turn_on_correct_led_for_temperature(self, temp):
        index = self.get_led_index_based_on_temperature(temp, self.temperature_ranges)

        if not index:
            self.__display.turn_on_all_leds()
        else:
            self.__display.turn_on_led_by_index(index)

    def write(self, values):
        relative_humidity, temperature = values

        self.__display.turn_off_all_leds()
        self.turn_on_correct_led_for_temperature(temperature)
