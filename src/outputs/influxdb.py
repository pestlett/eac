from os import environ
from influxdb import InfluxDBClient

class InfluxDB():
    def __init__(self, measurement='temperature', fields_formatter=None, client=InfluxDBClient):
        self.__measurement = measurement
        self.__fields_formatter = fields_formatter
        self.__client = client(
            host=environ.get('influxdb_host'),
            port=environ.get('influxdb_port'),
            username=environ.get('influxdb_username'),
            password=environ.get('influxdb_password'),
            database=environ.get('influxdb_database'))

    def __del__(self):
        self.__client.close()

    def create_fields(self, values):
        if self.__fields_formatter is None:
            relative_humidity, temperature = values

            return { 'relative_humidity': relative_humidity, 'temperature': temperature }

        return self.__fields_formatter(values)

    def create_point(self, values):
        return {
            'measurement': self.__measurement,
            'tags': {
                'machine': environ.get('machine_name', 'eac-machine'),
                'location': environ.get('machine_location', 'home'),
            },
            'fields': self.create_fields(values)
        }

    def write(self, values):
        self.__client.write_points([self.create_point(values)])
