from datetime import datetime

class Console():
    def __init__(self, measurement='temperature'):
        self.__measurement = measurement

    def write(self, values):
        relative_humidity, temperature = values

        print('\033c')
        print('EAC - Temperature Control')
        print('-------------------------')
        print('{}Z: Temperature={}C, Relative Humidity={}%'.format(
            datetime.utcnow(),
            temperature,
            relative_humidity))
