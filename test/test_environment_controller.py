import sys
import unittest
from unittest.mock import MagicMock

sys.modules['Adafruit_DHT'] = MagicMock()

from environment_controller import EnvironmentController

class TestEnvironmentController(unittest.TestCase):
    def mock_outputs_subscription(self, mock_subscription):
        for output in self.test_controller._EnvironmentController__outputs:
            output['subscription'] = mock_subscription

    def setUp(self):
        self.mock_stream = MagicMock()
        self.test_controller = EnvironmentController(channel=12)
        self.test_controller._EnvironmentController__stream = self.mock_stream

    def tearDown(self):
        self.mock_stream.reset_mock()
        self.test_controller.__del__()

    def test_connect(self):
        self.test_controller.connect()

        self.mock_stream.connect.assert_called()

    def test_add_output(self):
        mock_output = MagicMock()

        self.test_controller.add_output(mock_output)
        self.test_controller.add_output(mock_output)

        actual = len(self.test_controller._EnvironmentController__outputs)

        self.assertEqual(actual, 2)

    def test_add_output_subscribes_to_stream(self):
        mock_output = MagicMock()

        self.test_controller.add_output(mock_output)

        self.mock_stream.subscribe.assert_called_with(mock_output.write)

    def test_remove_output(self):
        mock_output = MagicMock()

        self.test_controller.add_output(mock_output)
        self.test_controller.remove_output(mock_output)

        actual = len(self.test_controller._EnvironmentController__outputs)

        self.assertEqual(actual, 0)

    def test_remove_output_unsubscribes_from_stream(self):
        mock_output = MagicMock()
        subscription = MagicMock()

        self.test_controller.add_output(mock_output)
        self.mock_outputs_subscription(subscription)
        self.test_controller.remove_output(mock_output)

        subscription.unsubscribe.assert_called()

    def test_remove_output_does_not_try_to_remove_if_filtered_list_is_empty(self):
        mock_output = MagicMock()
        self.test_controller._EnvironmentController____remove_and_unsubscribe_output = MagicMock()

        self.test_controller.add_output(mock_output)
        self.test_controller.remove_output('mock_output')

        self.test_controller._EnvironmentController____remove_and_unsubscribe_output.assert_not_called()

    def test_destructor_removes_all_outputs(self):
        mock_output = MagicMock()

        self.test_controller.add_output(mock_output)
        self.test_controller.add_output(mock_output)
        self.test_controller.__del__()

        actual = len(self.test_controller._EnvironmentController__outputs)

        self.assertEqual(actual, 0)

    def test_destructor_unsubscribes_all_outputs(self):
        mock_output = MagicMock()
        subscription = MagicMock()

        self.test_controller.add_output(mock_output)
        self.test_controller.add_output(mock_output)
        self.mock_outputs_subscription(subscription)
        self.test_controller.__del__()

        self.assertEqual(subscription.unsubscribe.call_count, 2)

if __name__ == '__main__':
    unittest.main()
