import unittest
from unittest.mock import MagicMock
from rx import Observable
from rx.testing import TestScheduler, ReactiveTest
from rx.testing import marbles

from devices.sensors.dht.am2302 import AM2302

on_next = ReactiveTest.on_next

class TestAM2302(unittest.TestCase):
    expected = ('humidity', 'temperature')

    def setUp(self):
        DHT = MagicMock()
        DHT.read.return_value = self.expected

        self.test_scheduler = TestScheduler()
        self.device = AM2302(channel=10, dht=DHT)

    def tearDown(self):
        del self.test_scheduler
        del self.device

    def test_read_value(self):
        self.assertEqual(self.device.read_value(0), self.expected)

    def test_are_all_readings_valid_is_false_when_a_None_reading_is_found(self):
        self.assertFalse(self.device.are_all_readings_valid((None, 'test')))

    def test_are_all_readings_valid_is_true_when_no_None_readings_are_found(self):
        self.assertTrue(self.device.are_all_readings_valid(('test', 'test')))

    def test_stream_readings_with_default_period(self):
        results = self.test_scheduler.start(
            lambda: self.device.stream_readings(scheduler=self.test_scheduler), disposed=6000)

        results.messages.assert_equal(
            on_next(2200, self.expected),
            on_next(4200, self.expected))

    def test_stream_readings_with_custom_period(self):
        results = self.test_scheduler.start(
            lambda: self.device.stream_readings(100, scheduler=self.test_scheduler), disposed=500)

        results.messages.assert_equal(
            on_next(300, self.expected),
            on_next(400, self.expected))

if __name__ == '__main__':
    unittest.main()
