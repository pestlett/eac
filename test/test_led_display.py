import sys
import unittest
from unittest.mock import MagicMock

from devices.diodes.led_mode import LEDMode
from devices.diodes.led import LED
from led_display import LEDDisplay

class TestLED(unittest.TestCase):
    def setUp(self):
        self.led_display = LEDDisplay()

    def tearDown(self):
        self.led_display.__del__()

    def test_get_leds_currently_off(self):
        self.led_display.add_led(LED(channel=21, gpio=MagicMock(), mode=LEDMode.OFF))
        self.led_display.add_led(LED(channel=22, gpio=MagicMock(), mode=LEDMode.OFF))
        self.led_display.add_led(LED(channel=23, gpio=MagicMock(), mode=LEDMode.ON))

        leds_currently_off = self.led_display.get_leds_currently_off()

        self.assertEqual(len(leds_currently_off), 2)

    def test_get_leds_currently_on(self):
        self.led_display.add_led(LED(channel=21, gpio=MagicMock(), mode=LEDMode.ON))
        self.led_display.add_led(LED(channel=22, gpio=MagicMock(), mode=LEDMode.ON))
        self.led_display.add_led(LED(channel=23, gpio=MagicMock(), mode=LEDMode.OFF))

        leds_currently_on = self.led_display.get_leds_currently_on()

        self.assertEqual(len(leds_currently_on), 2)

    def test_add_led_by_channel(self):
        self.led_display.add_led_by_channel(12)
        leds_currently_off = self.led_display.get_leds_currently_off()

        self.assertEqual(len(leds_currently_off), 1)

    def test_add_led_raises_type_error_when_none_led_is_passed(self):
        with self.assertRaises(TypeError):
            self.led_display.add_led('mock')

    def test_add_led_raises_value_error_when_the_same_channel_is_added_twice(self):
        self.led_display.add_led(LED(channel=1, gpio=MagicMock(), mode=LEDMode.ON))
        with self.assertRaises(ValueError):
            self.led_display.add_led(LED(channel=1, gpio=MagicMock(), mode=LEDMode.ON))

    def test_add_led(self):
        self.led_display.add_led(LED(channel=21, gpio=MagicMock(), mode=LEDMode.ON))
        leds_currently_on = self.led_display.get_leds_currently_on()

        self.assertEqual(len(leds_currently_on), 1)

    def test_turn_on_all_leds(self):
        self.led_display.add_led(LED(channel=21, gpio=MagicMock(), mode=LEDMode.OFF))
        self.led_display.add_led(LED(channel=22, gpio=MagicMock(), mode=LEDMode.OFF))
        self.led_display.add_led(LED(channel=23, gpio=MagicMock(), mode=LEDMode.OFF))

        self.led_display.turn_on_all_leds()

        leds = self.led_display.get_leds_currently_on()

        self.assertEqual(len(leds), 3)

    def test_turn_off_all_leds(self):
        self.led_display.add_led(LED(channel=21, gpio=MagicMock(), mode=LEDMode.ON))
        self.led_display.add_led(LED(channel=22, gpio=MagicMock(), mode=LEDMode.ON))
        self.led_display.add_led(LED(channel=23, gpio=MagicMock(), mode=LEDMode.ON))

        self.led_display.turn_off_all_leds()

        leds = self.led_display.get_leds_currently_off()

        self.assertEqual(len(leds), 3)

    def test_for_each(self):
        self.led_display.add_led_by_channel(1)
        self.led_display.add_led_by_channel(2)
        self.led_display.add_led_by_channel(3)

        channels = []

        self.led_display.for_each(lambda led: channels.append(led.channel))

        self.assertEqual(sum(channels, 0), 6)

    def test_turn_on_led_by_index(self):
        self.led_display.add_led_by_channel(1)
        self.led_display.add_led_by_channel(3)
        self.led_display.add_led_by_channel(2)

        self.led_display.turn_on_led_by_index(1)
        second_led_in_list = self.led_display.get_leds_currently_on().pop()

        self.assertEqual(second_led_in_list.channel, 3)

    def test_turn_off_led_by_index(self):
        self.led_display.add_led(LED(channel=1, gpio=MagicMock(), mode=LEDMode.ON))
        self.led_display.add_led(LED(channel=3, gpio=MagicMock(), mode=LEDMode.ON))
        self.led_display.add_led(LED(channel=2, gpio=MagicMock(), mode=LEDMode.ON))

        self.led_display.turn_off_led_by_index(1)
        second_led_in_list = self.led_display.get_leds_currently_off().pop()

        self.assertEqual(second_led_in_list.channel, 3)

if __name__ == '__main__':
    unittest.main()
