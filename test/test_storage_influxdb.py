import sys
import unittest
from unittest.mock import MagicMock, mock_open, patch

sys.modules['influxdb'] = MagicMock()

from outputs.influxdb import InfluxDB

class TestInfluxDB(unittest.TestCase):
    expected = ('humidity', 'temperature')

    def influx_db_client(self, **kwargs):
        return self.influx_db

    def setUp(self):
        self.influx_db = MagicMock()

    def tearDown(self):
        self.influx_db.reset_mock()

    def test_destructor_closes_the_connection(self):
        influx_db = InfluxDB(client=self.influx_db_client)

        influx_db.__del__()

        self.influx_db.close.assert_called()

    def test_create_fields_returns_correct_values_when_no_formatter_is_passed(self):
        influx_db = InfluxDB(client=self.influx_db_client)

        actual = influx_db.create_fields(('hum', 'temp'))

        self.assertEqual(actual, { 'relative_humidity': 'hum', 'temperature': 'temp' })

    def test_create_fields_calls_fields_formatter_when_it_is_passed(self):
        fields_formatter = MagicMock(return_value='temp')
        influx_db = InfluxDB(client=self.influx_db_client, fields_formatter=fields_formatter)

        expected = ('hum', 'temp')
        influx_db.create_fields(expected)

        fields_formatter.assert_called_with(expected)

    def test_create_point_uses_correctly_set_measurement(self):
        measurement='mock'
        influx_db = InfluxDB(client=self.influx_db_client, measurement=measurement)

        actual = influx_db.create_point(('hum', 'temp'))

        self.assertEqual(actual.get('measurement'), measurement)

    def test_write_calls_influx_with_correct_value(self):
        influx_db = InfluxDB(client=self.influx_db_client)
        mock_point_data = 'mock_data'

        influx_db.create_point = MagicMock(return_value=mock_point_data)
        influx_db.write(('hum', 'temp'))

        self.influx_db.write_points.assert_called_with([mock_point_data])

if __name__ == '__main__':
    unittest.main()
