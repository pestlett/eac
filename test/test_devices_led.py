import sys
import unittest
from unittest.mock import MagicMock

GPIO = MagicMock()

sys.modules['RPi'] = MagicMock()
sys.modules['RPi.GPIO'] = GPIO

from devices.diodes.led_mode import LEDMode
from devices.diodes.led import LED

class TestLED(unittest.TestCase):
    def setUp(self):
        self.GPIO = GPIO

    def tearDown(self):
        self.GPIO.reset_mock()

    def test_led_init(self):
        led = LED(channel=12, mode=LEDMode.ON, gpio=self.GPIO)

        self.assertEqual(led.channel, 12)
        self.assertEqual(led.mode, LEDMode.ON)

    def test_led_init_default_mode(self):
        led = LED(channel=13, gpio=self.GPIO)

        self.assertEqual(led.mode, LEDMode.OFF)

    def test_setting_channel_should_call_gpio_setup(self):
        led = LED(channel=10, gpio=self.GPIO)

        self.GPIO.setup.assert_called_with(10, self.GPIO.OUT)

    def test_cleanup_should_be_called_with_previously_set_channel(self):
        led = LED(channel=10, gpio=self.GPIO)

        led.channel = 13

        self.GPIO.cleanup.assert_called_with(10)

    def test_cleanup_channel__cleanup_is_called_when_channel_is_set(self):
        led = LED(channel=10, gpio=self.GPIO)

        led.cleanup_channel(10)

        self.GPIO.cleanup.assert_called_with(10)

    def test_cleanup_channel__cleanup_is_not_called_when_channel_is_not_set(self):
        led = LED(channel=None, gpio=self.GPIO)

        led.cleanup_channel(None)

        self.assertFalse(self.GPIO.cleanup.called)

    def test_update_device__output_is_called_with_correct_channel_and_mode(self):
        led = LED(channel=12, gpio=self.GPIO)

        led.update_device(12)

        self.GPIO.output.assert_called_with(12, LEDMode.OFF.value)

    def test_turn_on_sets_mode_to_ON(self):
        led = LED(channel=12, mode=LEDMode.OFF, gpio=self.GPIO)

        led.turn_on()

        self.assertEqual(led.mode, LEDMode.ON)

    def test_turn_off_sets_mode_to_OFF(self):
        led = LED(channel=12, mode=LEDMode.ON, gpio=self.GPIO)

        led.turn_off()

        self.assertEqual(led.mode, LEDMode.OFF)

    def test_toggle_sets_mode_to_OFF_when_mode_is_ON(self):
        led = LED(channel=12, mode=LEDMode.ON, gpio=self.GPIO)

        led.toggle()

        self.assertEqual(led.mode, LEDMode.OFF)

    def test_toggle_sets_mode_to_ON_when_mode_is_OFF(self):
        led = LED(channel=12, mode=LEDMode.ON, gpio=self.GPIO)

        led.toggle()

        self.assertEqual(led.mode, LEDMode.OFF)

if __name__ == '__main__':
    unittest.main()
