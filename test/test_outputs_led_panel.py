import sys
import unittest
from unittest.mock import MagicMock, mock_open, patch

from outputs.led_panel import LEDPanel

class TestLEDPanel(unittest.TestCase):
    temperature_ranges = [range(-274, 5), range(5, 16), range(16, 20), range(21, 23), range(23, 25), range(25, 274)]

    def led_panel_display(self, **kwargs):
        return self.led_panel

    def setUp(self):
        self.led_panel = MagicMock()

    def tearDown(self):
        self.led_panel.reset_mock()

    def test_get_led_index_based_on_temperature(self):
        led_panel = LEDPanel(channels=[11, 12])

        actual = led_panel.get_led_index_based_on_temperature(18, self.temperature_ranges)

        self.assertEqual(actual, 2)

    def test_get_led_index_based_on_temperature_returns_None_when_temperature_not_found(self):
        led_panel = LEDPanel(channels=[11, 12])

        actual = led_panel.get_led_index_based_on_temperature(275, self.temperature_ranges)

        self.assertEqual(actual, None)

    def test_turn_on_correct_led_for_temperature(self):
        led_panel = LEDPanel(display=self.led_panel_display, channels=[11, 12])
        mock_index = 6

        led_panel.get_led_index_based_on_temperature = MagicMock(return_value=mock_index)

        led_panel.turn_on_correct_led_for_temperature(1)

        self.led_panel.turn_on_led_by_index.assert_called_with(mock_index)

    def test_turn_on_correct_led_for_temperature_throws(self):
        led_panel = LEDPanel(display=self.led_panel_display, channels=[11, 12])

        led_panel.get_led_index_based_on_temperature = MagicMock(return_value=None)

        led_panel.turn_on_correct_led_for_temperature(1)

        self.led_panel.turn_on_all_leds.assert_called()

if __name__ == '__main__':
    unittest.main()
